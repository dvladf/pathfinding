#include "App.h"

#include <limits>
#include <queue>
#include <sstream>

App::App() : map(MAP_SIZE, Tile::None), savedMap(MAP_SIZE), rects(MAP_SIZE)
{
	window = SDL_CreateWindow("Pathfinding", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIN_SIZE.w, WIN_SIZE.h, 0);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_SOFTWARE);

	texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, NCOLS, NROWS);

	for (int i = 0; i < NCOLS; ++i)
	{
		map[i] = Tile::Wall;
		map[NCOLS * (NROWS - 1) + i] = Tile::Wall;
	}

	for (int i = 0; i < NROWS; ++i)
	{
		map[NCOLS*i] = Tile::Wall;
		map[NCOLS*(i + 1) - 1] = Tile::Wall;
	}

	SDL_UpdateTexture(texture, nullptr, map.data(), NCOLS * sizeof(Tile));

	static constexpr uint32_t blue = 0x0000FF00;
	static constexpr uint32_t red = 0xFF000000;

	startTexture = CreatePixelTexture(blue);
	endTexture = CreatePixelTexture(red);

	for (int i = 0; i < NCOLS; ++i)
	{
		for (int j = 0; j < NROWS; ++j)
		{
			SDL_Rect& rect = rects[i + j * NCOLS];
			rect.w = rect.h = CELL_SIZE;
			rect.x = i * CELL_SIZE;
			rect.y = j * CELL_SIZE;
		}
	}
}

App::~App()
{
	SDL_DestroyTexture(endTexture);
	SDL_DestroyTexture(startTexture);
	SDL_DestroyTexture(texture);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}


void App::Run()
{
    bool isRunning = true;
	SDL_Event evt;

	while (isRunning)
	{
		while (SDL_PollEvent(&evt))
		{
			switch (evt.type)
			{
			case SDL_QUIT:
				isRunning = false;
				break;
            case SDL_KEYUP:

                switch (evt.key.keysym.sym)
                {
                case SDLK_SPACE:
					if (pathFound)
						ClearPath();
					FindPath();
                    break;
                }

                break;
			}
		}

		Update();
		Render();
	}
}

void App::Update()
{
	int xpos, ypos;
	SDL_PumpEvents();

	Uint32 mouseState = SDL_GetMouseState(&xpos, &ypos);
	int i = xpos / CELL_SIZE + ypos / CELL_SIZE * NCOLS;

	if (mouseState & SDL_BUTTON(SDL_BUTTON_RIGHT))
	{
		if (!pathFound)
		{
			map[i] = map[i + 1] = map[i + NCOLS] = map[i + NCOLS + 1] = Tile::Wall;
			SDL_UpdateTexture(texture, nullptr, map.data(), NCOLS * sizeof(Tile));
		}
		else
			ClearPath();
	}
	else if ((mouseState & SDL_BUTTON(SDL_BUTTON_LEFT)) && map[i] != Tile::Wall)
	{
		if (!pathFound)
		{
			if (!start) start = i;
			else end = i;
		}
		else
			ClearPath();
	}
}

void App::Render()
{
	SDL_RenderClear(renderer);
	SDL_RenderCopy(renderer, texture, nullptr, &WIN_SIZE);

	if (start)
		SDL_RenderCopy(renderer, startTexture, nullptr, &rects[start]);
	
	if (end)
		SDL_RenderCopy(renderer, endTexture, nullptr, &rects[end]);

	if (isGrid)
	{
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);
		for (int x = 0; x < WIN_SIZE.w; x += CELL_SIZE)
			SDL_RenderDrawLine(renderer, x, 0, x, WIN_SIZE.h);
		for (int y = 0; y < WIN_SIZE.h; y += CELL_SIZE)
			SDL_RenderDrawLine(renderer, 0, y, WIN_SIZE.w, y);
	}

	SDL_RenderPresent(renderer);
}

constexpr std::array<std::pair<int, uint8_t>, 8> App::connections;

int App::Astar(std::function<unsigned int(int i)> heuristic)
{
    static constexpr auto INF = std::numeric_limits<unsigned int>::max();
    using Node = std::pair<unsigned int, int>; // Node: 1 - distance, 2 - index;

    std::vector<uint8_t> visited(MAP_SIZE, false);
    std::vector<unsigned int> distance(MAP_SIZE, INF);

    std::priority_queue<Node, std::vector<Node>, std::greater<Node>> queue;

    distance[start] = 0;
    queue.push(std::make_pair(0, start));

    int numVisited = 0;
    int nodeIndex;

    while (!queue.empty())
    {
        Node tempNode = queue.top();
        queue.pop();
        nodeIndex = tempNode.second;
        visited[nodeIndex] = true;
        map[nodeIndex] = Tile::Visited;
        numVisited++;

        if (nodeIndex == end)
        {
            break;
        }

        for (const auto& v : connections)
        {
            const int i = nodeIndex + v.first;
            const unsigned char weight = v.second;

            if (map[i] != Tile::Wall && !visited[i])
            {
                unsigned int tempDistance = distance[nodeIndex] + weight + heuristic(i);
                if (distance[i] > tempDistance)
                {
                    distance[i] = tempDistance;
                    queue.push(std::make_pair(distance[i], i));
                }
            }
        }
    }

	while (nodeIndex != start)
	{
		unsigned int minDistance = INF;
		int prevNodeIndex;
		for (const auto &v : connections)
		{
			if (distance[v.first + nodeIndex] < minDistance)
			{
				prevNodeIndex = v.first + nodeIndex;
				minDistance = distance[prevNodeIndex];
			}
		}
		map[prevNodeIndex] = Tile::Path;
		nodeIndex = prevNodeIndex;
	}

    return numVisited;
}

void App::FindPath()
{
	static constexpr SDL_MessageBoxButtonData buttons[]
	{
		{ 0, 1, "A*" },
		{ 0, 0, "Dijkstra" },
	};

	static constexpr SDL_MessageBoxData messageBoxData
	{
		SDL_MESSAGEBOX_INFORMATION,
		nullptr,
		"Pathfinding algorithm",
		"Choose a pathfinding algorithm.",
		SDL_arraysize(buttons),
		buttons,
		nullptr
	};

	int buttonId;
	SDL_ShowMessageBox(&messageBoxData, &buttonId);

	std::function<unsigned int(int i)> heuristic;

	switch (buttonId)
	{
	case 0:
		heuristic = [](int i) { return 0; };
		break;
	case 1:
		heuristic = [this](int i) 
		{  
			return abs(rects[i].x - rects[end].x) + abs(rects[i].y - rects[end].y);
		};
		break;
	}

	savedMap = map;

	Uint64 startCounter = SDL_GetPerformanceCounter();
	int numVisited = Astar(heuristic);
	Uint64 endCounter = SDL_GetPerformanceCounter();

	double findTime = (endCounter - startCounter) * 1000 / static_cast<double>(SDL_GetPerformanceFrequency());

	SDL_UpdateTexture(texture, nullptr, map.data(), NCOLS * sizeof(Tile));
	Render();

	std::stringstream ss;
	ss << "Visited nodes numbers: " << numVisited << '\n' << "Finding time: " << findTime << " ms" << '\n';
	SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, "Result", ss.str().c_str(), window);

	pathFound = true;
}