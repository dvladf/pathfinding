#pragma once

#include <array>
#include <functional>
#include <vector>
#include <SDL.h>

static constexpr SDL_Rect WIN_SIZE = { 0, 0, 800, 600 };
static constexpr int CELL_SIZE = 8;

class App
{
	static constexpr int NCOLS = WIN_SIZE.w / CELL_SIZE;
	static constexpr int NROWS = WIN_SIZE.h / CELL_SIZE;
	static constexpr int MAP_SIZE = NROWS * NCOLS;

	SDL_Window* window;
	SDL_Renderer* renderer;
	
	std::vector<SDL_Rect> rects;

	enum class Tile : uint32_t
	{
		None = 0x33993300,
		Wall = 0x3D5C5C00,
		Visited = 0x00CC0000,
		Path = 0xFFCC0000,
	};

	std::vector<Tile> map;
	std::vector<Tile> savedMap;
	SDL_Texture* texture;

	int start = 0, end = 0;
	SDL_Texture *startTexture, *endTexture;

    static constexpr std::array<std::pair<int, uint8_t>, 8> connections
    {
        std::make_pair(-NCOLS - 1, 3),
        std::make_pair(-NCOLS, 2),
        std::make_pair(-NCOLS +1, 3),
        std::make_pair(-1, 2),
        std::make_pair(1, 2),
        std::make_pair(NCOLS - 1, 3),
        std::make_pair(NCOLS, 2),
        std::make_pair(NCOLS + 1, 3)
    };

	bool pathFound = false;
	bool isGrid = true;

public:
	App();
	~App();
	void Run();

private:
	void Update();
	void Render();
	void FindPath();

    int Astar(std::function<unsigned int(int i)> heuristic);

	inline SDL_Texture* CreatePixelTexture(uint32_t pixel) 
	{ 
		SDL_Texture* texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, 1, 1);
		SDL_UpdateTexture(texture, nullptr, &pixel, sizeof(uint32_t));
		return texture;
	}

	inline void ClearPath()
	{
		map = savedMap;
		SDL_UpdateTexture(texture, nullptr, savedMap.data(), NCOLS * sizeof(Tile));
		pathFound = false;
	}
};