SOURCES=pathfinding.cpp
EXEC=pathfinding
CFLAGS=--std=c++14 -O2
SDL_CFLAGS=$(shell sdl2-config --cflags)
SDL_LIBS=$(shell sdl2-config --libs)
CC=g++

all:
	$(CC) $(SOURCES) $(CFLAGS) $(SDL_CFLAGS) -o $(EXEC) $(SDL_LIBS)

clean:
	rm $(EXEC)
